# coGSEA_SetRank
**co**mparative **G**ene **S**et **E**nrichment **Analysis**

## How to install

```
require(devtools)
devtools::install_git('https://mborry@gitlab.pasteur.fr/mborry/coGSEA_SetRank.git')
```
#### Disclaimer :
This tool is largely inspired by the [eGSEA](http://bioconductor.org/packages/release/bioc/html/EGSEA.html) R package (and contains some of its code)
